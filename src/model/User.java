package model;

public class User {
	
	private String username;
	private String password;
	private userType userType;
	
	public User(String username, String password, userType userType) {
		super();
		this.username = username;
		this.password = password;
		this.userType = userType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
		
	public userType getUserType() {
		return userType;
	}

}
