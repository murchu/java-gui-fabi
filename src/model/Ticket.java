package model;

import java.util.Date;
import java.util.Random;

public class Ticket {
    
	private int id;
	private Date created;
	private User assignee;
	private String detail;
	public issuePriority priority;
	private issueStatus status;
	
	
	// Ticket constructor w/ only detail known
	public Ticket(String detail) {
		super();
		this.id = new Random().nextInt();
		this.created = new Date();
		this.detail = detail;
		this.status = issueStatus.OPEN;
	}

	// Ticket constructor w/ detail & priority known
	public Ticket(String detail, issuePriority priority) {
		super();
		this.id = new Random().nextInt();
		this.created = new Date();
		this.detail = detail;
		this.priority = priority;
		this.status = issueStatus.OPEN;
	}

	public Ticket(User assignee, String detail, issuePriority priority) {
		super();
		this.id = new Random().nextInt();
		this.created = new Date();
		this.assignee = assignee;
		this.detail = detail;
		this.priority = priority;
		this.status = issueStatus.OPEN;
	}


	public User getAssignee() {
		return assignee;
	}

	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public issuePriority getPriority() {
		return priority;
	}

	public void setPriority(issuePriority priority) {
		this.priority = priority;
	}

	public int getId() {
		return id;
	}

	public Date getCreated() {
		return created;
	}
	
}