package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ManagerDashboard extends JFrame implements ActionListener {
	
	JFrame appFrame;
	Container cPane;
	
	JLabel totalTicketsLabel;
	JLabel totalNumberLabel;
	JLabel totalCostLabel;
	
	JLabel openTicketsLabel;
	JLabel totalOpenLabel;
	JLabel openCostLabel;
	
	JLabel closedTicketsLabel;
	JLabel totalClosedLabel;
	JLabel closedCostLabel;
	
	JTextField assigneeInput;
	JTextField issueDetailInput;
	JTextField priorityInput;
	
	JButton createTicketButton;
	
	GridBagConstraints gridBagConstraints;
	
	//Default constructor for this class
	public ManagerDashboard() {
		/*Create a frame, get its contentpane and set layout*/
	    appFrame = new JFrame("Tech Support System | Manager Dashboard");
	    cPane = appFrame.getContentPane();
	    cPane.setLayout(new GridBagLayout());

	    //Arrange components on contentPane and set Action Listeners to each JButton
	    //arrangeComponents();
	    
	    // initialise components
	    totalTicketsLabel = new JLabel("Total Tickets");
	    totalNumberLabel = new JLabel("No. tickets: " + 5);
	    totalCostLabel = new JLabel("Cost: E" + 250);
	    
	    openTicketsLabel = new JLabel("Open Tickets");
	    totalOpenLabel = new JLabel("No. tickets: " + 2);
	    openCostLabel = new JLabel("Cost: E" + 100);    
	    
	    closedTicketsLabel = new JLabel("Closed Tickets");
	    totalClosedLabel = new JLabel("No. tickets: " + 3);
	    closedCostLabel = new JLabel("Cost: E" + 150);
	    
	    	 	    
	    // add components to screen
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(totalTicketsLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(totalNumberLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(totalCostLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 2;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(openTicketsLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 3;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(totalOpenLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 3;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(openCostLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 4;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(closedTicketsLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 5;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(totalClosedLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 5;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(closedCostLabel, gridBagConstraints);
	    
	    appFrame.setSize(400,300);
	    appFrame.setResizable(false);
	    appFrame.setVisible(true);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createTicketButton) {
			//loginWithDatabase();
		}
		
	}

	public static void main(String[] args) {
		new ManagerDashboard();
	}

}
