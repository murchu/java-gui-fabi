package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AdminDashboard extends JFrame implements ActionListener {
	
	JFrame appFrame;
	Container cPane;
	
	JButton createNewUser;
	JButton changeUserPassword;
	
	GridBagConstraints gridBagConstraints;
	
	//Default constructor for this class
	public AdminDashboard() {
		/*Create a frame, get its contentpane and set layout*/
	    appFrame = new JFrame("Tech Support System | Admin Dashboard");
	    cPane = appFrame.getContentPane();
	    cPane.setLayout(new GridBagLayout());

	    //Arrange components on contentPane and set Action Listeners to each JButton
	    //arrangeComponents();
	    
	    // initialise components    
	    createNewUser = new JButton("Create New User");
	    changeUserPassword = new JButton("Change User Password");
	    
	    // add components to screen
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(createNewUser, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(changeUserPassword, gridBagConstraints);
	   	    
	    createNewUser.addActionListener(this);
	    changeUserPassword.addActionListener(this);
	    
	    appFrame.setSize(400,300);
	    appFrame.setResizable(false);
	    appFrame.setVisible(true);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createNewUser) {
			//loginWithDatabase();
		}
		if (e.getSource() == changeUserPassword) {
			//loginWithDatabase();
		}
	}

	public static void main(String[] args) {
		new AdminDashboard();
	}

}
