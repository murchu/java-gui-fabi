package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.TicketController;
import model.Ticket;

public class SupportDashboard extends JFrame implements ActionListener {
	
	ArrayList<Ticket> allTickets;
	
	JFrame appFrame;
	Container cPane;
	
	JLabel idLabel;
	JLabel createdLabel;
	JLabel assigneeLabel;
	JLabel detailLabel;
	JLabel priorityLabel;
	
	JButton createTicketButton;
	JButton deleteTicketButton;

	
	GridBagConstraints gridBagConstraints;
		
	//Default constructor for this class
	public SupportDashboard() {
		/*Create a frame, get its contentpane and set layout*/
	    appFrame = new JFrame("Tech Support System | Support Dashboard");
	    cPane = appFrame.getContentPane();
	    cPane.setLayout(new GridBagLayout());

	    //Arrange components on contentPane and set Action Listeners to each JButton
	    //arrangeComponents();
	    
	    // initialise components
	    idLabel = new JLabel("ticketId");
	    createdLabel = new JLabel("dateCreated");
	    assigneeLabel = new JLabel("assignedTo");
	    detailLabel = new JLabel("issueDetail");
	    priorityLabel = new JLabel("priority");
	    
	    createTicketButton = new JButton("Create New Ticket");
	    deleteTicketButton = new JButton("Delete Ticket");
	    
	    // add components to screen
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(idLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(createdLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 2;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(assigneeLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 3;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(detailLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 4;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(priorityLabel, gridBagConstraints);
	    
	    int currentTicketRow = 0;
	    allTickets = TicketController.getAllTickets();
	    
	    for (Ticket t : allTickets) {
	    	gridBagConstraints = new GridBagConstraints();
		    gridBagConstraints.gridx = 0;
		    gridBagConstraints.gridy = currentTicketRow + 1;
		    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
		    cPane.add(new JLabel(String.valueOf(t.getId())), gridBagConstraints);
		    
		    gridBagConstraints = new GridBagConstraints();
		    gridBagConstraints.gridx = 1;
		    gridBagConstraints.gridy = currentTicketRow + 1;
		    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
		    cPane.add(new JLabel(String.valueOf(t.getCreated())), gridBagConstraints);
		    
		    gridBagConstraints = new GridBagConstraints();
		    gridBagConstraints.gridx = 2;
		    gridBagConstraints.gridy = currentTicketRow + 1;
		    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
		    cPane.add(new JLabel(String.valueOf(t.getAssignee().getUsername())), gridBagConstraints);
		    
		    gridBagConstraints = new GridBagConstraints();
		    gridBagConstraints.gridx = 3;
		    gridBagConstraints.gridy = currentTicketRow + 1;
		    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
		    cPane.add(new JLabel(String.valueOf(t.getDetail())), gridBagConstraints);
		    
		    gridBagConstraints = new GridBagConstraints();
		    gridBagConstraints.gridx = 4;
		    gridBagConstraints.gridy = currentTicketRow + 1;
		    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
		    cPane.add(new JLabel(String.valueOf(t.getPriority())), gridBagConstraints);
		    
		    currentTicketRow += 1;
	    }
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = currentTicketRow + 2;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(createTicketButton, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 2;
	    gridBagConstraints.gridy = currentTicketRow + 2;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(deleteTicketButton, gridBagConstraints);
	    
	    //loginButton.addActionListener(this);
	    
	    appFrame.setSize(800,300);
	    appFrame.setResizable(false);
	    appFrame.setVisible(true);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createTicketButton) {
			//loginWithDatabase();
		}
		if (e.getSource() == deleteTicketButton) {
			//loginWithDatabase();
		}
		
	}

	public static void main(String[] args) {
		new SupportDashboard();
	}

}
