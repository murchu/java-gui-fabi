package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AdminAddUser extends JFrame implements ActionListener {
	
	JFrame appFrame;
	Container cPane;
	
	JLabel usernameLabel;
	JLabel passwordLabel;
	
	JTextField usernameInput;
	JTextField passwordInput;
	
	JButton addUserButton;
	
	GridBagConstraints gridBagConstraints;
	
	//Default constructor for this class
	public AdminAddUser() {
		/*Create a frame, get its contentpane and set layout*/
	    appFrame = new JFrame("Tech Support System | Create User");
	    cPane = appFrame.getContentPane();
	    cPane.setLayout(new GridBagLayout());

	    //Arrange components on contentPane and set Action Listeners to each JButton
	    //arrangeComponents();
	    
	    // initialise components
	    usernameLabel = new JLabel("Username");
	    passwordLabel = new JLabel("Password");
	    usernameInput = new JTextField(20);
	    passwordInput = new JTextField(20);
	    addUserButton = new JButton("Create User");
	    
	    // add components to screen
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(usernameLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(usernameInput, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(passwordLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(passwordInput, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 2;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(addUserButton, gridBagConstraints);
	    
	    addUserButton.addActionListener(this);
	    
	    appFrame.setSize(400,300);
	    appFrame.setResizable(false);
	    appFrame.setVisible(true);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addUserButton) {
			//loginWithDatabase();
		}
		
	}

	public static void main(String[] args) {
		new AdminAddUser();
	}

}
