package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class LogoutScreen extends JFrame implements ActionListener {
	
	JFrame appFrame;
	Container cPane;
	
	JLabel confirmLogoutLabel;

	JButton logoutYesButton;
	JButton logoutNoButton;
	
	GridBagConstraints gridBagConstraints;
	
	//Default constructor for this class
	public LogoutScreen() {
		/*Create a frame, get its contentpane and set layout*/
	    appFrame = new JFrame("Tech Support System | Confirm Logout");
	    cPane = appFrame.getContentPane();
	    cPane.setLayout(new GridBagLayout());

	    //Arrange components on contentPane and set Action Listeners to each JButton
	    //arrangeComponents();
	    
	    // initialise components
	    confirmLogoutLabel = new JLabel("Do you want to logout?");
	    logoutYesButton = new JButton("Yes");
	    logoutNoButton = new JButton("No");
	    
	    // add components to screen
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(confirmLogoutLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(logoutYesButton, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(logoutNoButton, gridBagConstraints);
	    	    
	    logoutYesButton.addActionListener(this);
	    logoutNoButton.addActionListener(this);
	    
	    appFrame.setSize(400,300);
	    appFrame.setResizable(false);
	    appFrame.setVisible(true);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == logoutYesButton) {
			//loginWithDatabase();
		}
		
		if (e.getSource() == logoutNoButton) {
			//loginWithDatabase();
		}
		
	}

	public static void main(String[] args) {
		new LogoutScreen();
	}

}
