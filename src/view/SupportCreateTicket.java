package view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class SupportCreateTicket extends JFrame implements ActionListener {
	
	JFrame appFrame;
	Container cPane;
	
	JLabel assigneeLabel;
	JLabel issueDetailLabel;
	JLabel priorityLabel;
	
	JTextField assigneeInput;
	JTextField issueDetailInput;
	JTextField priorityInput;
	
	JButton createTicketButton;
	
	GridBagConstraints gridBagConstraints;
	
	//Default constructor for this class
	public SupportCreateTicket() {
		/*Create a frame, get its contentpane and set layout*/
	    appFrame = new JFrame("Tech Support System | Create New Ticket");
	    cPane = appFrame.getContentPane();
	    cPane.setLayout(new GridBagLayout());

	    //Arrange components on contentPane and set Action Listeners to each JButton
	    //arrangeComponents();
	    
	    // initialise components
	    assigneeLabel = new JLabel("Assignee");
	    issueDetailLabel = new JLabel("Details");
	    priorityLabel = new JLabel("Priority");
	    assigneeInput = new JTextField(20);
	    issueDetailInput = new JTextField(20);
	    priorityInput = new JTextField(20);	    
	    createTicketButton = new JButton("Create Ticket");
	    
	    // add components to screen
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(assigneeLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(issueDetailLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 0;
	    gridBagConstraints.gridy = 2;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(priorityLabel, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 0;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(assigneeInput, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 1;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(issueDetailInput, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 2;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(priorityInput, gridBagConstraints);
	    
	    gridBagConstraints = new GridBagConstraints();
	    gridBagConstraints.gridx = 1;
	    gridBagConstraints.gridy = 3;
	    gridBagConstraints.insets = new Insets(5, 5, 5, 5);
	    cPane.add(createTicketButton, gridBagConstraints);
	    
	    createTicketButton.addActionListener(this);
	    
	    appFrame.setSize(400,300);
	    appFrame.setResizable(false);
	    appFrame.setVisible(true);
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createTicketButton) {
			//loginWithDatabase();
		}
		
	}

	public static void main(String[] args) {
		new SupportCreateTicket();
	}

}
