package controller;

import java.util.ArrayList;

import model.Ticket;
import model.User;
import model.issuePriority;
import model.userType;

public class TicketController {
	
	public static ArrayList<Ticket> getAllTickets() {
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		
		User john = new User("johno", "1234", userType.SUPPORT);
		User fred = new User("freddie", "2345", userType.SUPPORT);
		
		Ticket t1 = new Ticket(john, "World ended!", issuePriority.URGENT);
		tickets.add(t1);
		Ticket t2 = new Ticket(fred, "I'm lonely", issuePriority.LONGTERM);
		tickets.add(t2);
		Ticket t3 = new Ticket(john, "Is it fixed yet?!", issuePriority.URGENT);
		tickets.add(t3);
				
		return tickets;
	}

	// TODO getTotalNumberTickets()
	// TODO getNumberOpenTickets()
	// TODO getNumberClosedTickets()
	// TODO getCostTotalTickets()
	// TODO getCostOpenTickets()
	// TODO getCostClosedTickets()

}
